# Copyright 2021 Huawei Technologies Co., Ltd
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
Metrics module
"""
from abc import abstractmethod
import numpy as np


class BaseMetrics:
    """The abstract base class for metrics classes"""

    @abstractmethod
    def collect(self, outputs, labels):
        """
        Collect and process the outputs of the model each inference.
        Args:
            outputs (numpy.ndarray): The outputs of models.
            labels (numpy.ndarray): The labels of data.

        Raises:
            NotImplementedError: It is an abstract method.
        """
        msg = "The function collect() is an abstract method in class 'BaseMetrics', " \
              "and should be implemented in child class"
        raise NotImplementedError(msg)

    @abstractmethod
    def metrics(self):
        """
        Metrics the final result.

        Raises:
            NotImplementedError: It is an abstract method.
        """
        msg = "The function metrics() is an abstract method in class 'BaseMetrics', " \
              "and should be implemented in child class"
        raise NotImplementedError(msg)


class ClassifierMetrics(BaseMetrics):
    """Implementation of classifier metrics."""
    def __init__(self):
        """Initiated."""
        self.total_num = 0
        self.correct_num = 0
        self.acc = 0

    def collect(self, outputs, labels):
        """
        Collect and process the outputs of the model each inference.
        Args:
            outputs (numpy.ndarray): The outputs of models.
            labels (numpy.ndarray): The labels of data.
        """
        predict = np.argmax(outputs, axis=1)
        mask = np.equal(predict, labels)
        self.total_num += mask.shape[0]
        self.correct_num += np.sum(mask)

    def metrics(self):
        """
        Metrics the final result.

        Returns:
            float : Accuracy.
        """
        self.acc = self.correct_num / self.total_num
        return self.acc
